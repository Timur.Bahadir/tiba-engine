#include <tiba/tiba.hpp>

int main() {
	tiba::app::Application app{"./game_config.json"};
	app.start();

	return 0;
}
