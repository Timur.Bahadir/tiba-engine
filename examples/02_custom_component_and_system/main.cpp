#include <tiba/tiba.hpp>

//
// Component
//
struct Bounce {
	bool enabled{true};
	tiba::core::IDimension extents{};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Bounce, enabled, extents);

void BounceFactory(entt::registry &registry, entt::entity entity, json const &data) {
	registry.emplace<Bounce>(entity, data.get<Bounce>());
}

tiba::ecs::ComponentFactoryPair BounceComponentPair{"Bounce", BounceFactory};

//
// System
//
class BounceSystem : public tiba::ecs::BaseSystem {
private:
	tiba::core::IDimension bounds{};

public:
	BounceSystem(tiba::ecs::SystemConfigData const &config)
			: BaseSystem{config}, bounds{config.window->get_window_size()} {}
	~BounceSystem() = default;

	void operator()(tiba::real_t) {
		auto view = _registry->view<tiba::ecs::components::Position, tiba::ecs::components::Velocity, Bounce>();

		view.each([this](tiba::ecs::components::Position &pos, tiba::ecs::components::Velocity &vel, Bounce &bounce) {
			if (!bounce.enabled) {
				return;
			}

			if (pos.x - (bounce.extents.width / 2.0) < 0) {
				vel.x *= -1;
				pos.x = bounce.extents.width / 2.0;
			}
			if (pos.x + bounce.extents.width > bounds.width) {
				vel.x *= -1;
				pos.x = bounds.width - bounce.extents.width;
			}
			if (pos.y - (bounce.extents.height / 2.0) < 0) {
				vel.y *= -1;
				pos.y = (bounce.extents.height / 2.0);
			}
			if (pos.y + bounce.extents.height > bounds.height) {
				vel.y *= -1;
				pos.y = bounds.height - bounce.extents.height;
			}
		});
	}
};

tiba::ecs::BaseSystem *BounceSystemFactory(tiba::ecs::SystemConfigData const &config) {
	return new BounceSystem(config);
}

tiba::ecs::SystemFactoryPair BounceSystemPair{"BounceSystem", BounceSystemFactory};

class SpawnSystem : public tiba::ecs::BaseSystem {
private:
	tiba::core::IDimension bounds{};
	bool _auto_spawn{false};

	void spawn_random_ball() {
		using namespace tiba;
		auto e = _registry->create();

		auto const mp = _input->get_mouse_poition();
		ecs::components::Position position{mp.x, mp.y};

		core::Shape shape = util::rand::get_random_shape(core::RDimension{32, 32}, 0.5f);
		ecs::components::Shape shape_component{shape};

		auto const random_dir =
				util::rand::get_random_vector<tiba::real_t>(util::rand::get_random_value_in_range(-100, 100));
		ecs::components::Velocity velocity{random_dir.x, random_dir.y};
		Bounce bounce{true, core::IDimension{32, 32}};

		_registry->emplace<ecs::components::Position>(e, position);
		_registry->emplace<ecs::components::Velocity>(e, velocity);
		_registry->emplace<ecs::components::Shape>(e, shape_component);
		_registry->emplace<Bounce>(e, bounce);
	}

public:
	SpawnSystem(tiba::ecs::SystemConfigData const &config)
			: BaseSystem{config}, bounds{config.window->get_window_size()} {}
	~SpawnSystem() = default;

	void operator()(tiba::real_t) {
		using namespace tiba;

		if (_input->is_mouse_button_just_down(input::MouseButton::Left)) {
			spawn_random_ball();
		}

		if (_input->is_mouse_button_just_down(input::MouseButton::Right)) {
			for (std::uint8_t i{0}; i < 10; ++i) {
				spawn_random_ball();
			}
		}

		if (_input->is_key_just_down(input::Key::W)) {
			_auto_spawn = !_auto_spawn;
		}

		if (_input->is_key_down(input::Key::R)) {
			_registry->clear();
		}

		if (_auto_spawn) {
			for (std::uint8_t i{0}; i < 5; ++i) {
				spawn_random_ball();
			}
		}
	}
};

tiba::ecs::BaseSystem *SpawnSystemFactory(tiba::ecs::SystemConfigData const &config) { return new SpawnSystem(config); }

tiba::ecs::SystemFactoryPair SpawnSystemPair{"SpawnSystem", SpawnSystemFactory};

int main() {
	tiba::app::Application app{"./game_config.json", {BounceSystemPair, SpawnSystemPair}, {BounceComponentPair}};
	app.start();

	return 0;
}
