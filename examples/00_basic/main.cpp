#include <iostream>

#include <tiba/tiba.hpp>

int main() {

	tiba::size_t count{100};

	std::cout << "Hello World " << count << " times!" << std::endl;
	return 0;
}
