#include <tiba/tiba.hpp>

//
// Component
//
struct PlayerInput {
	tiba::real_t speed{};
	tiba::input::Key left{};
	tiba::input::Key right{};
	tiba::input::Key up{};
	tiba::input::Key down{};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(PlayerInput, speed, left, right, up, down);

void PlayerInputFactory(entt::registry &registry, entt::entity entity, json const &data) {
	registry.emplace<PlayerInput>(entity, data.get<PlayerInput>());
}

tiba::ecs::ComponentFactoryPair PlayerInputComponentPair{"PlayerInput", PlayerInputFactory};

//
// System
//
class PlayerInputSystem : public tiba::ecs::BaseSystem {
public:
	PlayerInputSystem(tiba::ecs::SystemConfigData const &config) : BaseSystem(config) {}

	void operator()(tiba::real_t) {
		auto view = _registry->view<tiba::ecs::components::Position, tiba::ecs::components::Velocity, PlayerInput>();

		view.each([this](tiba::ecs::components::Position &pos, tiba::ecs::components::Velocity &vel, PlayerInput &playerInput) {
			tiba::core::RVector2 movement{};
			if (_input->is_key_down(playerInput.up)) {
				movement.y -= 1.0;
			}
			if (_input->is_key_down(playerInput.down)) {
				movement.y += 1.0;
			}
			if (_input->is_key_down(playerInput.left)) {
				movement.x -= 1.0;
			}
			if (_input->is_key_down(playerInput.right)) {
				movement.x += 1.0;
			}
			movement.normalize();
			movement *= playerInput.speed;
			vel.x = movement.x;
			vel.y = movement.y;
		});
	}
};

tiba::ecs::BaseSystem *PlayerInputSystemFactory(tiba::ecs::SystemConfigData const &config) {
	return new PlayerInputSystem(config);
}

tiba::ecs::SystemFactoryPair PlayerInputSystemPair{"PlayerInputSystem", PlayerInputSystemFactory};

int main() {

	tiba::app::Application app{"./game_config.json", {PlayerInputSystemPair}, {PlayerInputComponentPair}};
	app.start();

	return 0;
}
