# tiba game engine

Don't use this yet. More experiment than anything.

## Architecture

Layer based architecture

core -> media -> ecs -> app

## Concepts

### Entity

"Container" for Components

### Components

Data that can be attached to an Entity. Used by Systems.

### Systems

Contain logic and use Components as Data.

### World

State of the game. Including the ECS registry.

### Archtype

A template for Entities consisting of Components

### Scene

Describes a set of Systems to be active and Archtypes to be initialized.
