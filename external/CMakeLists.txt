cmake_minimum_required(VERSION 3.16)

add_subdirectory("entt")
add_subdirectory("json")
add_subdirectory("raylib")
add_subdirectory("random")
add_subdirectory("spdlog")
add_subdirectory("fmt")
add_subdirectory("doctest")
