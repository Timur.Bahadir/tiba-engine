#include "EnTTWorld.hpp"

#include <iomanip>
#include <iostream>
#include <memory>
#include <string>

#include <tiba/core/basetypes.hpp>
#include <tiba/core/entt.hpp>
#include <tiba/ecs/components/components.hpp>
#include <tiba/ecs/systems/systems.hpp>

#include <fmt/core.h>
#include <spdlog/spdlog.h>

namespace tiba::ecs {

std::unique_ptr<ecs::IWorld> create_world() { return std::make_unique<EnTTWorld>(); }

EnTTWorld::EnTTWorld() : _registry{}, _component_factory_map{} {
	spdlog::info("Creating EnTTWorld");

	_system_factory_map["MovementSystem"] = systems::MovementSystemFactory;
	_system_factory_map["RenderSystem"] = systems::RenderSystemFactory;

	_component_factory_map["Position"] = components::PositionFactory;
	_component_factory_map["Shape"] = components::ShapeFactory;
	_component_factory_map["Sprite"] = components::SpriteFactory;
	_component_factory_map["Velocity"] = components::VelocityFactory;
}

EnTTWorld::~EnTTWorld() {}

void EnTTWorld::register_system_factories(std::vector<SystemFactoryPair> const &factories) {
	for (auto &&factory : factories) {
		spdlog::info("Registering system factory {}", factory.first);
		_system_factory_map.insert(factory);
	}
}

void EnTTWorld::register_component_factories(std::vector<ComponentFactoryPair> const &factories) {
	for (auto &&factory : factories) {
		spdlog::info("Registering component factory {}", factory.first);
		_component_factory_map.insert(factory);
	}
}

bool EnTTWorld::create_entity(json const &data) {
	auto const entity = _registry.create();

	for (auto it = data.begin(); it != data.end(); ++it) {
		std::string const component_name{it.key()};

		auto const factory = _component_factory_map[component_name];
		if (factory == nullptr) {
			spdlog::error("No component factory for [{}] registered", component_name);
			continue;
		}
		factory(_registry, entity, it.value());
	}

	return true;
}

void EnTTWorld::update(tiba::real_t const delta) {
	for (auto &&system : _systems) {
		(*system)(delta);
	}
}

void EnTTWorld::clear_state() {
	spdlog::debug("Clearing world state");
	_registry.clear();
}

void EnTTWorld::load_scene(std::filesystem::path const &scene_path, media::IWindow *window, input::IInput *input) {
	spdlog::info("Loading scene {}", scene_path.string());

	auto const scene_config_data = core::read_json(scene_path);
	auto const scene_config = scene_config_data.get<SceneConfigData>();

	_create_systems(scene_config, window, input);

	for (auto &&entity : scene_config_data["entities"]) {
		create_entity(entity);
	}
}

void EnTTWorld::_create_systems(SceneConfigData const &scene_config, media::IWindow *window, input::IInput *input) {
	spdlog::info("Creating systems");

	SystemConfigData const system_config_data{&_registry, window, input};

	for (auto &&system_name : scene_config.systems) {
		spdlog::info("Creating system {}", system_name);

		auto const factory = _system_factory_map[system_name];
		if (factory == nullptr) {
			spdlog::error("No system factory for [{}] registered", system_name);
			continue;
		}
		auto system = factory(system_config_data);
		_systems.emplace_back(system);
	}
}

} // namespace tiba::ecs
