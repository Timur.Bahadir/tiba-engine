#include <tiba/core/helper.hpp>

#include <fstream>

namespace tiba::core {

std::string read_file(std::filesystem::path const &path) {
	std::ifstream ifs{path};
	std::string content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
	return content;
}

json read_json(std::filesystem::path const &path) {
	std::ifstream in{path};
	json out;
	in >> out;
	return out;
}

} // namespace tiba::core
