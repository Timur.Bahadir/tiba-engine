#pragma once

#include <tiba/core/core.hpp>
#include <tiba/media/IWindow.hpp>

#include "RaylibTextureLoader.hpp"

namespace tiba::media {

class RaylibWindow : public IWindow {
public:
	RaylibWindow(WindowSettings const &dimension);
	~RaylibWindow();

	bool is_open() override;
	core::IDimension get_window_size() override;

	void pre_draw() override;
	void post_draw() override;

	void draw_shape(core::RVector2 const &position, core::Shape const &shape) override;
	void draw_texture(media::Texture &texture, core::RVector2 const &position, tiba::real_t rotation, tiba::real_t scale,
										core::Color tint) override;

private:
	// TODO: move the cache to application? if so const texture in draw call
	entt::resource_cache<::Texture2D, media::RaylibTextureLoader> _texture_cache;
};

} // namespace tiba::media
