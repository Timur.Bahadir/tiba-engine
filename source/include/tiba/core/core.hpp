#pragma once

#include "Color.hpp"
#include "Dimension.hpp"
#include "Shape.hpp"
#include "Vector2.hpp"
#include "basetypes.hpp"
#include "helper.hpp"
#include "json.hpp"
