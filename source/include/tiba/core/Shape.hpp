#pragma once

#include <vector>

#include "Vector2.hpp"
#include "basetypes.hpp"
#include "json.hpp"

namespace tiba::core {

enum class ShapeType {
	Invalid,
	Line,
	Ellipse,
	Rectangle,
	Triangle,
	Last,
};

NLOHMANN_JSON_SERIALIZE_ENUM(ShapeType, {
																						{ShapeType::Invalid, nullptr},
																						{ShapeType::Line, "Line"},
																						{ShapeType::Ellipse, "Ellipse"},
																						{ShapeType::Rectangle, "Rectangle"},
																						{ShapeType::Triangle, "Triangle"},
																						{ShapeType::Last, "Last"},
																				})

struct Shape {
	core::ShapeType type{};
	core::RDimension size{};
	core::Color color{};
	bool filled{};
	std::vector<core::RVector2> points{};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Shape, type, size, color, filled, points);

} // namespace tiba::core
