#pragma once

#include <cstddef>
#include <cstdint>

namespace tiba {

using size_t = std::size_t;
using real_t = float;
using int_t = std::intmax_t;

} // namespace tiba
