#pragma once

#include <filesystem>
#include <fstream>
#include <string>

#include "json.hpp"

namespace tiba::core {

std::string read_file(std::filesystem::path const &path);

json read_json(std::filesystem::path const &path);

} // namespace tiba::core
