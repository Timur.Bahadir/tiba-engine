#pragma once

#include "basetypes.hpp"

#include "json.hpp"

namespace tiba::core {

template <typename T> struct BasicDimension {
	T width{};
	T height{};

	BasicDimension() : BasicDimension{0, 0} {}
	BasicDimension(T iw, T ih) : width{iw}, height{ih} {}

	BasicDimension operator*(T factor) const { return BasicDimension{width * factor, height * factor}; }
};

using IDimension = BasicDimension<tiba::size_t>;
using RDimension = BasicDimension<tiba::real_t>;

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(IDimension, width, height)
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(RDimension, width, height)

} // namespace tiba::core
