#pragma once

#include <cmath>
#include <iosfwd>
#include <string>

#include "basetypes.hpp"
#include "json.hpp"

#include <fmt/core.h>

namespace tiba::core {

namespace {

template <typename T> constexpr T get_epsilon() { return 0; }
template <> constexpr float get_epsilon() { return FLT_EPSILON; }
template <> constexpr double get_epsilon() { return DBL_EPSILON; }
template <> constexpr long double get_epsilon() { return LDBL_EPSILON; }

// http://realtimecollisiondetection.net/blog/?p=89
template <typename T> bool compare_real(T const lhs, T const rhs) {
	return std::abs(lhs - rhs) <= get_epsilon<T>() * std::max({1.0f, std::abs(lhs), std::abs(rhs)});
}

} // namespace

template <typename T> struct BasicVector2 {
	T x{};
	T y{};

	BasicVector2(T nx, T ny) : x{nx}, y{ny} {}
	BasicVector2() : x{0}, y{0} {}

	BasicVector2 &normalize() {
		T const xx = x * x;
		T const yy = y * y;
		T const length = sqrt(xx + yy);

		if (length == 0) {
			x = 0;
			y = 0;
		} else {
			x /= length;
			y /= length;
		}

		return *this;
	}

	BasicVector2 as_normalized() const {
		BasicVector2 copy{x, y};
		copy.normalize();
		return copy;
	}

	bool operator==(BasicVector2 other) const { return compare_real(x, other.x) && compare_real(y, other.y); }

	BasicVector2 operator*(T factor) const {
		BasicVector2<T> temp{x * factor, y * factor};
		return temp;
	}

	BasicVector2 &operator*=(T factor) {
		this->x *= factor;
		this->y *= factor;
		return *this;
	}

	friend std::ostream &operator<<(std::ostream &out, BasicVector2 vec) {
		out << fmt::format("[x:{},y:{}]", vec.x, vec.y);
		return out;
	}
};

using IVector2 = BasicVector2<tiba::int_t>;
using RVector2 = BasicVector2<tiba::real_t>;

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(IVector2, x, y)
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(RVector2, x, y)

} // namespace tiba::core
