#pragma once

#include "json.hpp"

#include <cstdint>

namespace tiba::core {

struct Color {
	std::uint8_t r{255};
	std::uint8_t g{255};
	std::uint8_t b{255};
	std::uint8_t a{255};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Color, r, g, b, a)

} // namespace tiba::core
