#pragma once

#include <string>
#include <vector>

#include <tiba/core/json.hpp>

namespace tiba::ecs {

struct SceneConfigData {
	std::vector<std::string> systems;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(SceneConfigData, systems);

} // namespace tiba::ecs
