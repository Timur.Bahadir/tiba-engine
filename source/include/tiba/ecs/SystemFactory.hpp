#pragma once

#include <string>
#include <utility>

#include <tiba/ecs/BaseSystem.hpp>

namespace tiba::ecs {

using SystemFactoryFunction = BaseSystem *(*)(SystemConfigData const &config);

using SystemFactoryPair = std::pair<std::string, SystemFactoryFunction>;

} // namespace tiba::ecs
