#pragma once

#include <string>
#include <utility>

#include <tiba/core/json.hpp>

#include <tiba/core/entt.hpp>

namespace tiba::ecs {

using ComponentFactoryFunction = void (*)(entt::registry &, entt::entity, json const &);

using ComponentFactoryPair = std::pair<std::string, ComponentFactoryFunction>;

} // namespace tiba::ecs
