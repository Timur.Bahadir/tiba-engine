#pragma once

#include <tiba/ecs/BaseSystem.hpp>

namespace tiba::ecs::systems {

class MovementSystem : public BaseSystem {
public:
	MovementSystem(SystemConfigData const &config);

	void operator()(tiba::real_t delta) override;
};

BaseSystem *MovementSystemFactory(SystemConfigData const &config);

} // namespace tiba::ecs::systems
