#pragma once

#include <tiba/ecs/BaseSystem.hpp>

namespace tiba::ecs::systems {

class RenderSystem : public BaseSystem {
public:
	RenderSystem(SystemConfigData const &config);

	void operator()(tiba::real_t delta) override;
};

BaseSystem *RenderSystemFactory(SystemConfigData const &config);

} // namespace tiba::ecs::systems
