#pragma once

#include <memory>
#include <vector>

#include <tiba/core/core.hpp>
#include <tiba/input/IInput.hpp>
#include <tiba/media/media.hpp>

#include "ComponentFactory.hpp"
#include "SystemFactory.hpp"

namespace tiba::ecs {

class IWorld {
public:
	IWorld() = default;
	virtual ~IWorld() = default;

	virtual void register_system_factories(std::vector<SystemFactoryPair> const &factories) = 0;

	virtual void register_component_factories(std::vector<ComponentFactoryPair> const &factories) = 0;

	virtual bool create_entity(json const &data) = 0;

	virtual void update(tiba::real_t const delta) = 0;

	virtual void clear_state() = 0;
	virtual void load_scene(std::filesystem::path const &scene_path, media::IWindow *window, input::IInput *input) = 0;
};

std::unique_ptr<IWorld> create_world();

} // namespace tiba::ecs
