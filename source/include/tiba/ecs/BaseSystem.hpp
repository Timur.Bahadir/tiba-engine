#pragma once

#include <tiba/core/basetypes.hpp>
#include <tiba/core/entt.hpp>
#include <tiba/input/IInput.hpp>
#include <tiba/media/IWindow.hpp>

namespace tiba::ecs {

struct SystemConfigData {
	entt::registry *registry;
	media::IWindow *window;
	input::IInput *input;
};

class BaseSystem {
public:
	BaseSystem(SystemConfigData const &config)
			: _registry{config.registry}, _window{config.window}, _input{config.input} {}
	virtual ~BaseSystem() = default;

	virtual void operator()(tiba::real_t delta) = 0;

protected:
	entt::registry *_registry{nullptr};
	media::IWindow *_window{nullptr};
	input::IInput *_input{nullptr};
};

} // namespace tiba::ecs
