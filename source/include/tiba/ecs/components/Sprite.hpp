#pragma once

#include <any>
#include <string>

#include <tiba/core/core.hpp>
#include <tiba/core/entt.hpp>
#include <tiba/media/Texture.hpp>

namespace tiba::ecs::components {

struct Sprite {
	std::string texture_path{};
	// TODO: Rotation and scale belong in the Position/Transform maybe
	tiba::real_t rotation{0.0f};
	tiba::real_t scale{1.0f};
	core::Color tint{255, 255, 255, 255};

	media::Texture _texture{};

	void _prepare(entt::registry &, entt::entity) { _texture.prepare(texture_path); }
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Sprite, texture_path, rotation, scale, tint);

constexpr entt::hashed_string SpriteComponentName{"Sprite"};

void SpriteFactory(entt::registry &registry, entt::entity entity, json const &data);

} // namespace tiba::ecs::components
