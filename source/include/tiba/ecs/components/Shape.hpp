#pragma once

#include <tiba/core/entt.hpp>
#include <tiba/core/json.hpp>

namespace tiba::ecs::components {

struct Shape {
	core::Shape shape_data;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Shape, shape_data);

constexpr entt::hashed_string ShapeComponentName{"Shape"};

void ShapeFactory(entt::registry &registry, entt::entity entity, json const &data);

} // namespace tiba::ecs::components
