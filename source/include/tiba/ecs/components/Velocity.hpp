#pragma once

#include <tiba/core/core.hpp>

#include <tiba/core/entt.hpp>

namespace tiba::ecs::components {

struct Velocity {
	tiba::real_t x{};
	tiba::real_t y{};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Velocity, x, y);

constexpr entt::hashed_string VelocityComponentName{"Velocity"};

void VelocityFactory(entt::registry &registry, entt::entity entity, json const &data);

} // namespace tiba::ecs::components
