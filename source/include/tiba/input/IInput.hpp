#pragma once

#include <tiba/core/Vector2.hpp>

#include "Key.hpp"
#include "MouseButton.hpp"

namespace tiba::input {

class IInput {
public:
	virtual ~IInput() = default;

	virtual bool is_key_down(Key key) = 0;
	virtual bool is_key_up(Key key) = 0;
	virtual bool is_key_just_down(Key key) = 0;
	virtual bool is_key_just_release(Key key) = 0;

	virtual bool is_mouse_button_down(MouseButton mouse_button) = 0;
	virtual bool is_mouse_button_up(MouseButton mouse_button) = 0;
	virtual bool is_mouse_button_just_down(MouseButton mouse_button) = 0;
	virtual bool is_mouse_button_just_release(MouseButton mouse_button) = 0;

	virtual core::RVector2 get_mouse_poition() = 0;
	virtual core::RVector2 get_mouse_delta() = 0;
	virtual tiba::real_t get_mouse_wheel_delta() = 0;
};

std::unique_ptr<IInput> create_input();

} // namespace tiba::input
