#pragma once

#include <tiba/core/json.hpp>

namespace tiba::input {

enum class MouseButton {
	Left,
	Right,
	Middle,
	Side,
	Extra,
	Forward,
	Back,
	Last,
};

NLOHMANN_JSON_SERIALIZE_ENUM(MouseButton, {
																							{MouseButton::Left, "Left"},
																							{MouseButton::Right, "Right"},
																							{MouseButton::Middle, "Middle"},
																							{MouseButton::Side, "Side"},
																							{MouseButton::Extra, "Extra"},
																							{MouseButton::Forward, "Forward"},
																							{MouseButton::Back, "Back"},
																							{MouseButton::Last, "Last"},
																					})

} // namespace tiba::input
