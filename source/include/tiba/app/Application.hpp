#pragma once

#include <chrono>
#include <filesystem>
#include <memory>

#include <tiba/core/core.hpp>
#include <tiba/ecs/ComponentFactory.hpp>
#include <tiba/ecs/IWorld.hpp>
#include <tiba/ecs/SystemFactory.hpp>
#include <tiba/input/IInput.hpp>
#include <tiba/media/IWindow.hpp>

namespace tiba::app {

class Application {
public:
	Application(std::filesystem::path const &config_path);
	Application(std::filesystem::path const &config_path,
							std::vector<ecs::SystemFactoryPair> const &custom_system_factories);
	Application(std::filesystem::path const &config_path,
							std::vector<ecs::SystemFactoryPair> const &custom_system_factories,
							std::vector<ecs::ComponentFactoryPair> const &custom_component_factories);
	~Application();

	void start();

private:
	json _config;

	std::unique_ptr<media::IWindow> _window;
	std::unique_ptr<input::IInput> _input;
	std::unique_ptr<ecs::IWorld> _ecs_world;

	std::chrono::time_point<std::chrono::high_resolution_clock> _last_frame_time;
};

} // namespace tiba::app
