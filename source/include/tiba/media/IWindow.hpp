#pragma once

#include <memory>
#include <string>

#include <tiba/core/core.hpp>
#include <tiba/media/Texture.hpp>

namespace tiba::media {

struct WindowSettings {
	std::string title;
	core::IDimension dimensions;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(WindowSettings, title, dimensions)

class IWindow {
public:
	IWindow() = default;
	virtual ~IWindow() = default;

	virtual bool is_open() = 0;
	virtual core::IDimension get_window_size() = 0;

	virtual void pre_draw() = 0;
	virtual void post_draw() = 0;

	// TODO: swap arg order
	virtual void draw_shape(core::RVector2 const &position, core::Shape const &shape) = 0;
	virtual void draw_texture(media::Texture &texture, core::RVector2 const &position, tiba::real_t rotation,
														tiba::real_t scale, core::Color tint) = 0;
};

std::unique_ptr<IWindow> create_window(WindowSettings const &window_settings);

} // namespace tiba::media
