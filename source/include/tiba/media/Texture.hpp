#pragma once

#include <any>
#include <filesystem>

#include <tiba/core/entt.hpp>

namespace tiba::media {

class Texture {
public:
	Texture();

	void prepare(std::filesystem::path const &path);
	void assign(entt::any const &data);

	bool is_loaded() const;

	entt::hashed_string id() const;
	entt::any const &get() const;
	std::filesystem::path const &path() const;

private:
	bool _is_loaded{false};
	entt::hashed_string _id;
	entt::any _representation;
	std::filesystem::path _path;
};

} // namespace tiba::media
