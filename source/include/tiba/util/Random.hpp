#pragma once

#include <type_traits>

#include <tiba/core/core.hpp>

#include <effolkronium/random.hpp>

namespace tiba::util::rand {

using Random = effolkronium::random_static;

template <typename T> T get_random_value() { return Random::get<T>(); }
template <typename T> T get_random_value(T limit) { return Random::get<T>(0, limit); }

template <typename T> T get_random_value_in_range(T from, T to) { return Random::get<T>(from, to); }

bool get_random_bool(tiba::real_t bias = 0.5f) { return Random::get<bool>(bias); }

core::Color get_random_color() {
	std::uint8_t const r{Random::get<std::uint8_t>()};
	std::uint8_t const g{Random::get<std::uint8_t>()};
	std::uint8_t const b{Random::get<std::uint8_t>()};

	return core::Color{r, g, b, 255};
}

template <typename T> core::BasicVector2<T> get_random_vector(T length) {
	auto const x{get_random_value_in_range<T>(-1, 1)};
	auto const y{get_random_value_in_range<T>(-1, 1)};
	core::BasicVector2<T> const vec{x, y};
	auto const normalized = vec.as_normalized();
	auto const ret{normalized * length};
	return ret;
}

core::ShapeType get_random_shape_type() {
	using base = std::underlying_type_t<core::ShapeType>;
	// 0 = Invalid and we don't want Last either
	auto const index = get_random_value_in_range<base>(1, static_cast<base>(core::ShapeType::Last) - 1);
	return static_cast<core::ShapeType>(index);
}

core::Shape get_random_shape(core::RDimension const &max_size, tiba::real_t size_range) {
	core::Shape shape{};
	shape.type = get_random_shape_type();

	auto const is_square = get_random_bool(0.3f);
	auto const size_factor = get_random_value_in_range(0.0f, 1.0f);
	auto const scaled_size = max_size * (1.0f + (size_factor * size_range));
	if (is_square) {
		shape.size = core::RDimension{scaled_size.width, scaled_size.width};
	} else {
		shape.size = scaled_size;
	}

	shape.color = get_random_color();

	shape.filled = get_random_bool();

	if (shape.type == core::ShapeType::Line) {
		auto const count = get_random_value_in_range<tiba::size_t>(1, 5);
		shape.points.reserve(count);

		shape.points.emplace_back(0.0f, 0.0f);

		for (tiba::size_t i = 1; i < count; ++i) {
			auto const last = shape.points.back();
			shape.points.emplace_back(last.x + ((i % 2) * i * 10.0f), last.y + (((i % 2) + 1) * i * 10.0f));
		}
	} else if (shape.type == core::ShapeType::Triangle) {
		shape.points.reserve(3);
		shape.points.emplace_back(-scaled_size.width / 2.0f, -scaled_size.height / 2.0f);
		shape.points.emplace_back(0, scaled_size.height / 2.0f);
		shape.points.emplace_back(scaled_size.width / 2.0f, -scaled_size.height / 2.0f);
	}

	return shape;
}

} // namespace tiba::util::rand
