#pragma once

#include <tiba/core/entt.hpp>
#include <tiba/ecs/BaseSystem.hpp>
#include <tiba/ecs/ComponentFactory.hpp>
#include <tiba/ecs/IWorld.hpp>
#include <tiba/ecs/SceneConfigData.hpp>
#include <tiba/ecs/SystemFactory.hpp>

#include <memory>
#include <unordered_map>
#include <vector>

namespace tiba::ecs {

class EnTTWorld : public ecs::IWorld {
public:
	EnTTWorld();
	~EnTTWorld();

	void register_system_factories(std::vector<SystemFactoryPair> const &factories) override;

	void register_component_factories(std::vector<ComponentFactoryPair> const &factories) override;

	bool create_entity(json const &data) override;

	void update(tiba::real_t const delta) override;

	void clear_state() override;

	void load_scene(std::filesystem::path const &scene_path, media::IWindow *window, input::IInput *input) override;

private:
	void _create_systems(SceneConfigData const &scene_config, media::IWindow *window, input::IInput *input);

	entt::registry _registry;

	std::vector<std::unique_ptr<ecs::BaseSystem>> _systems;

	// TODO: chose a faster hash-map, at least for component factories
	std::unordered_map<std::string, SystemFactoryFunction> _system_factory_map;
	std::unordered_map<std::string, ComponentFactoryFunction> _component_factory_map;
};

} // namespace tiba::ecs
