#include <tiba/ecs/components/components.hpp>

namespace tiba::ecs::components {

void PositionFactory(entt::registry &registry, entt::entity entity, json const &data) {
	auto const initial = data.get<Position>();
	registry.emplace<Position>(entity, initial);
}

void VelocityFactory(entt::registry &registry, entt::entity entity, json const &data) {
	auto const initial = data.get<Velocity>();
	registry.emplace<Velocity>(entity, initial);
}

void ShapeFactory(entt::registry &registry, entt::entity entity, json const &data) {
	auto const initial = data.get<Shape>();
	registry.emplace<Shape>(entity, initial);
}

void SpriteFactory(entt::registry &registry, entt::entity entity, json const &data) {
	auto const initial = data.get<Sprite>();
	registry.emplace<Sprite>(entity, initial);
}

} // namespace tiba::ecs::components
