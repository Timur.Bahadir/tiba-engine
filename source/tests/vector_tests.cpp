#include <doctest/doctest.h>

#include <tiba/core/Vector2.hpp>

#include <sstream>
#include <string>

TEST_SUITE("[tiba] Vector2") {
	TEST_CASE("Default Initialization") {
		tiba::core::RVector2 vec{};

		REQUIRE(vec.x == doctest::Approx(0.0));
		REQUIRE(vec.y == doctest::Approx(0.0));
	}

	TEST_CASE("Value Initialization") {
		tiba::real_t x = 10.0f;
		tiba::real_t y = 20.0f;

		tiba::core::RVector2 vec{x, y};

		REQUIRE(vec.x == doctest::Approx(x));
		REQUIRE(vec.y == doctest::Approx(y));
	}

	TEST_CASE("Comparison") {
		tiba::core::RVector2 vec1{0.1f + 0.2f, 0.123f};

		SUBCASE("Match") {
			tiba::core::RVector2 vec2{0.3f, 0.123f};
			REQUIRE(vec1 == vec2);
		}

		SUBCASE("Mismatch") {
			tiba::core::RVector2 vec2{20.0f, 100.0f};
			REQUIRE_FALSE(vec1 == vec2);
		}

		SUBCASE("Approx match") {
			tiba::core::RVector2 vec1{0.300000012f, 0.300000012f};

			SUBCASE("X") {
				tiba::core::RVector2 vec2{0.300000013f, 0.300000012f};
				REQUIRE(vec1 == vec2);
			}
			SUBCASE("Y") {
				tiba::core::RVector2 vec2{0.300000012f, 0.300000013f};
				REQUIRE(vec1 == vec2);
			}
			SUBCASE("X and Y") {
				tiba::core::RVector2 vec2{0.300000013f, 0.300000013f};
				REQUIRE(vec1 == vec2);
			}
		}
	}

	TEST_CASE("Normalization") {
		tiba::core::RVector2 vec{-54.3f, 232.67f};
		tiba::core::RVector2 const copy{vec};

		SUBCASE("Normalize") {
			vec.normalize();

			REQUIRE(vec.x == doctest::Approx(-0.22727f));
			REQUIRE(vec.y == doctest::Approx(0.97383f));

			REQUIRE(copy.x == doctest::Approx(-54.3f));
			REQUIRE(copy.y == doctest::Approx(232.67f));
		}

		SUBCASE("As normalized") {
			auto const normalized = vec.as_normalized();

			REQUIRE(normalized.x == doctest::Approx(-0.22727f));
			REQUIRE(normalized.y == doctest::Approx(0.97383f));

			REQUIRE(vec == copy);
		}

		SUBCASE("Zero length") {
			tiba::core::RVector2 vec{0, 0};
			vec.normalize();
			// Don't explode in the line above
			REQUIRE(vec.x == 0);
			REQUIRE(vec.y == 0);
		}
	}

	TEST_CASE("Scalar multiplication") {
		auto const x = -54.3f;
		auto const y = 232.67f;
		auto const scalar = 3.4f;

		tiba ::core::RVector2 vec{x, y};
		tiba::core::RVector2 const copy{vec};

		SUBCASE("* operator") {
			auto const scaled = vec * scalar;

			REQUIRE(scaled.x == x * scalar);
			REQUIRE(scaled.y == y * scalar);

			REQUIRE(vec == copy);
		}

		SUBCASE("*= operator") {
			vec *= scalar;

			REQUIRE(vec.x == x * scalar);
			REQUIRE(vec.y == y * scalar);

			REQUIRE_FALSE(vec == copy);
		}
	}

	TEST_CASE("Stringify") {
		tiba::core::RVector2 vec1{13.3f, 8.4f};

		SUBCASE("<< operator") {
			std::stringstream ss{};
			ss << vec1;
			std::string string = ss.str();
			REQUIRE("[x:13.3,y:8.4]" == string);
		}
	}

	TEST_CASE("JSON De-/Serialization") {
		SUBCASE("From JSON") {
			tiba::core::RVector2 const expected{15.3f, 22.6f};
			std::string json_string = R"({"x": 15.3, "y": 22.6})";
			REQUIRE(json::accept(json_string));

			json json_data = json::parse(json_string);
			tiba::core::RVector2 const parsed = json_data.get<tiba::core::RVector2>();
			REQUIRE(expected == parsed);
		}

		SUBCASE("To JSON") {
			tiba::core::RVector2 const vec{12.5f, 33.5f};
			std::string expected = R"({"x":12.5,"y":33.5})";
			json json_data = vec;

			std::string json_string = json_data.dump();
			REQUIRE(expected == json_string);
		}
	}
}
