#pragma once

#include <filesystem>
#include <memory>
#include <string>

#include <tiba/core/entt.hpp>

#include <raylib.h>
#include <spdlog/spdlog.h>

namespace tiba::media {

struct RaylibTextureLoader final {
	using result_type = std::shared_ptr<::Texture2D>;

	result_type operator()(std::filesystem::path const &path) const {
		spdlog::debug("Loading texture {} into cache", path.string());

		auto const stack = LoadTexture(path.string().c_str());
		std::shared_ptr<::Texture2D> heap{new ::Texture2D()};
		heap->id = stack.id;
		heap->format = stack.format;
		heap->width = stack.width;
		heap->height = stack.height;
		heap->mipmaps = stack.mipmaps;
		return heap;
	}
};

} // namespace tiba::media
