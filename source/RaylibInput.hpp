#pragma once

#include <tiba/input/IInput.hpp>

namespace tiba::input {

class RaylibInput : public IInput {
public:
	RaylibInput();
	~RaylibInput() = default;

	bool is_key_down(Key key) override;
	bool is_key_up(Key key) override;
	bool is_key_just_down(Key key) override;
	bool is_key_just_release(Key key) override;

	bool is_mouse_button_down(MouseButton mouse_button) override;
	bool is_mouse_button_up(MouseButton mouse_button) override;
	bool is_mouse_button_just_down(MouseButton mouse_button) override;
	bool is_mouse_button_just_release(MouseButton mouse_button) override;

	core::RVector2 get_mouse_poition() override;
	core::RVector2 get_mouse_delta() override;
	tiba::real_t get_mouse_wheel_delta() override;
};

} // namespace tiba::input
