#include <tiba/app/Application.hpp>

#include <tiba/core/core.hpp>

#include <spdlog/spdlog.h>

namespace tiba::app {

Application::Application(std::filesystem::path const &config_path)
		: Application{config_path, std::vector<ecs::SystemFactoryPair>{}, std::vector<ecs::ComponentFactoryPair>{}} {}

Application::Application(std::filesystem::path const &config_path,
												 std::vector<ecs::SystemFactoryPair> const &custom_system_factories)
		: Application{config_path, custom_system_factories, std::vector<ecs::ComponentFactoryPair>{}} {}

Application::Application(std::filesystem::path const &config_path,
												 std::vector<ecs::SystemFactoryPair> const &custom_system_factories,
												 std::vector<ecs::ComponentFactoryPair> const &custom_component_factories) {
	// TODO: Logging settings
	spdlog::info("Creating application");

	_config = core::read_json(config_path);

	auto const window_settings = _config["window_settings"].get<media::WindowSettings>();

	_window = media::create_window(window_settings);
	_input = input::create_input();
	_ecs_world = ecs::create_world();
	_ecs_world->register_system_factories(custom_system_factories);
	_ecs_world->register_component_factories(custom_component_factories);
}

Application::~Application() {}

void Application::start() {
	spdlog::info("Starting application");

	_ecs_world->load_scene(_config["entry_scene_path"], _window.get(), _input.get());

	while (_window->is_open()) {
		auto const now = std::chrono::high_resolution_clock::now();
		auto const difference = now - _last_frame_time;

		auto const delta = std::chrono::duration<tiba::real_t>(difference).count();

		_window->pre_draw();
		// TODO: do the accumulator thing
		_ecs_world->update(delta);
		_window->post_draw();

		_last_frame_time = now;
	}
}

} // namespace tiba::app
