#include "RaylibWindow.hpp"

#include <algorithm>
#include <iostream>

#include <raylib.h>

#include <spdlog/spdlog.h>

using namespace entt::literals;

namespace tiba::media {

std::unique_ptr<IWindow> create_window(WindowSettings const &window_settings) {
	return std::make_unique<RaylibWindow>(window_settings);
}

RaylibWindow::RaylibWindow(WindowSettings const &window_settings) : _texture_cache{} {
	spdlog::info("Creating RaylibWindow");

	InitWindow(static_cast<int>(window_settings.dimensions.width), static_cast<int>(window_settings.dimensions.height),
						 window_settings.title.c_str());

	// TODO: From window settings
	SetTargetFPS(60);
}

RaylibWindow::~RaylibWindow() { CloseWindow(); }

bool RaylibWindow::is_open() { return !WindowShouldClose(); }

core::IDimension RaylibWindow::get_window_size() {
	auto const ww = static_cast<tiba::size_t>(GetScreenWidth());
	auto const wh = static_cast<tiba::size_t>(GetScreenHeight());
	return core::IDimension{ww, wh};
}

void RaylibWindow::pre_draw() {
	BeginDrawing();
	// TODO: Get clear color from scene config
	ClearBackground(RAYWHITE);
}

void RaylibWindow::post_draw() { EndDrawing(); }

void RaylibWindow::draw_shape(core::RVector2 const &position, core::Shape const &shape) {
	::Color const shape_color{shape.color.r, shape.color.g, shape.color.b, shape.color.a};
	core::BasicVector2<int> const center{static_cast<int>(position.x), static_cast<int>(position.y)};
	core::BasicDimension<int> const extents{static_cast<int>(shape.size.width), static_cast<int>(shape.size.height)};

	std::vector<::Vector2> points{};
	if (shape.points.size() > 0) {
		points.reserve(shape.points.size());

		// TODO: we have to do this every call...
		std::transform(shape.points.begin(), shape.points.end(), std::back_inserter(points),
									 [&center](core::RVector2 const &vec) {
										 return ::Vector2{center.x + vec.x, center.y + vec.y};
									 });
	}

	switch (shape.type) {
	case core::ShapeType::Line:
		DrawLineStrip(points.data(), static_cast<int>(points.size()), shape_color);
		break;
	case core::ShapeType::Ellipse:
		if (shape.filled) {
			DrawEllipse(center.x, center.y, shape.size.width, shape.size.height, shape_color);
		} else {
			DrawEllipseLines(center.x, center.y, shape.size.width, shape.size.height, shape_color);
		}
		break;
	case core::ShapeType::Rectangle:
		if (shape.filled) {
			DrawRectangle(center.x, center.y, extents.width, extents.height, shape_color);
		} else {
			DrawRectangleLines(center.x, center.y, extents.width, extents.height, shape_color);
		}
		break;
	case core::ShapeType::Triangle:
		if (points.size() < 3) {
			spdlog::error("Trying to draw triangle shape with less than 3 points");
			return;
		}
		if (shape.filled) {
			DrawTriangle(points[0], points[1], points[2], shape_color);
		} else {
			DrawTriangleLines(points[0], points[1], points[2], shape_color);
		}
		break;
	default:
		break;
	}
}

void RaylibWindow::draw_texture(media::Texture &texture, core::RVector2 const &position, tiba::real_t rotation,
																tiba::real_t scale, core::Color tint) {
	::Vector2 const draw_position{position.x, position.y};
	::Color const draw_tint_color{tint.r, tint.g, tint.b, tint.a};

	if (!texture.is_loaded()) {
		auto const result = _texture_cache.load(texture.id(), texture.path());
		auto const resource = *result.first;
		auto const raylib_texture = *resource.second;
		texture.assign(raylib_texture);
	}

	auto const draw_texture = entt::any_cast<Texture2D>(texture.get());
	DrawTextureEx(draw_texture, draw_position, rotation, scale, draw_tint_color);
}

} // namespace tiba::media
