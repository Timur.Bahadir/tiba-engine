#pragma once

#include <tiba/input/MouseButton.hpp>

#include <array>

#include <raylib.h>

using RaylibMouseButtonMap =
		std::array<decltype(MOUSE_BUTTON_LEFT), static_cast<std::size_t>(tiba::input::MouseButton::Last)>;

constexpr RaylibMouseButtonMap raylib_mouse_button_map{
		MOUSE_BUTTON_LEFT,  MOUSE_BUTTON_RIGHT,   MOUSE_BUTTON_MIDDLE, MOUSE_BUTTON_SIDE,
		MOUSE_BUTTON_EXTRA, MOUSE_BUTTON_FORWARD, MOUSE_BUTTON_BACK,
};
