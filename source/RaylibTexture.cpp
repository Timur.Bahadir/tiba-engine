#include <tiba/media/Texture.hpp>

#include <string>

#include <raylib.h>

namespace tiba::media {

Texture::Texture() : _is_loaded{false} {}

void Texture::prepare(std::filesystem::path const &path) {
	_path = path;
	_id = entt::hashed_string{_path.string().c_str()};
}

void Texture::assign(entt::any const &data) {
	_representation = data;
	_is_loaded = true;
}

bool Texture::is_loaded() const { return _is_loaded; }

entt::hashed_string Texture::id() const { return _id; }

std::filesystem::path const &Texture::path() const { return _path; }

entt::any const &Texture::get() const { return _representation; }

} // namespace tiba::media
