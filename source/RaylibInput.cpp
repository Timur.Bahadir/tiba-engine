#include "RaylibInput.hpp"

#include <array>

#include "RaylibKeyMap.hpp"
#include "RaylibMouseButtonMap.hpp"

#include <raylib.h>
#include <spdlog/spdlog.h>

namespace tiba::input {

namespace {

inline int get_raylib_key(Key key) { return raylib_key_map[static_cast<std::size_t>(key)]; }

inline int get_raylib_mouse_button(MouseButton button) {
	return raylib_mouse_button_map[static_cast<std::size_t>(button)];
}

} // namespace

std::unique_ptr<IInput> create_input() { return std::make_unique<RaylibInput>(); }

RaylibInput::RaylibInput() { spdlog::info("Creating RaylibInput"); }

bool RaylibInput::is_key_down(Key key) { return IsKeyDown(get_raylib_key(key)); }
bool RaylibInput::is_key_up(Key key) { return IsKeyUp(get_raylib_key(key)); }
bool RaylibInput::is_key_just_down(Key key) { return IsKeyPressed(get_raylib_key(key)); }
bool RaylibInput::is_key_just_release(Key key) { return IsKeyReleased(get_raylib_key(key)); }

bool RaylibInput::is_mouse_button_down(MouseButton mouse_button) {
	return IsMouseButtonDown(get_raylib_mouse_button(mouse_button));
}
bool RaylibInput::is_mouse_button_up(MouseButton mouse_button) {
	return IsMouseButtonUp(get_raylib_mouse_button(mouse_button));
}
bool RaylibInput::is_mouse_button_just_down(MouseButton mouse_button) {
	return IsMouseButtonPressed(get_raylib_mouse_button(mouse_button));
}
bool RaylibInput::is_mouse_button_just_release(MouseButton mouse_button) {
	return IsMouseButtonReleased(get_raylib_mouse_button(mouse_button));
}

core::RVector2 RaylibInput::get_mouse_poition() {
	auto const rp = GetMousePosition();
	return core::RVector2{static_cast<tiba::real_t>(rp.x), static_cast<tiba::real_t>(rp.y)};
}
core::RVector2 RaylibInput::get_mouse_delta() {
	auto const rp = GetMouseDelta();
	return core::RVector2{static_cast<tiba::real_t>(rp.x), static_cast<tiba::real_t>(rp.y)};
}
tiba::real_t RaylibInput::get_mouse_wheel_delta() { return static_cast<tiba::real_t>(GetMouseWheelMove()); }

} // namespace tiba::input
