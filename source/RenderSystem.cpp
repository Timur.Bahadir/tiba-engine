#include <tiba/ecs/systems/RenderSystem.hpp>

#include <tiba/ecs/components/Position.hpp>
#include <tiba/ecs/components/Shape.hpp>
#include <tiba/ecs/components/Sprite.hpp>

namespace tiba::ecs::systems {

RenderSystem::RenderSystem(SystemConfigData const &config) : BaseSystem{config} {
	config.registry->on_construct<components::Sprite>().connect<entt::invoke<&components::Sprite::_prepare>>();
}

void RenderSystem::operator()(tiba::real_t) {
	auto shape_view = _registry->view<components::Position, components::Shape>();
	auto sprite_view = _registry->view<components::Position, components::Sprite>();

	shape_view.each([this](components::Position &pos, components::Shape &shape) {
		_window->draw_shape(core::RVector2{pos.x, pos.y}, shape.shape_data);
	});

	sprite_view.each([this](components::Position &pos, components::Sprite &sprite) {
		_window->draw_texture(sprite._texture, core::RVector2{pos.x, pos.y}, sprite.rotation, sprite.scale, sprite.tint);
	});
}

BaseSystem *RenderSystemFactory(SystemConfigData const &config) { return new RenderSystem(config); }

} // namespace tiba::ecs::systems
