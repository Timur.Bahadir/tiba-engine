#include <tiba/ecs/systems/MovementSystem.hpp>

#include <tiba/ecs/components/Position.hpp>
#include <tiba/ecs/components/Velocity.hpp>

namespace tiba::ecs::systems {

MovementSystem::MovementSystem(SystemConfigData const &config) : BaseSystem{config} {}

void MovementSystem::operator()(tiba::real_t delta) {
	auto view = _registry->view<components::Position, components::Velocity>();

	view.each([delta](components::Position &pos, components::Velocity &vel) {
		pos.x += vel.x * delta;
		pos.y += vel.y * delta;
	});
}

BaseSystem *MovementSystemFactory(SystemConfigData const &config) { return new MovementSystem(config); }

} // namespace tiba::ecs::systems
